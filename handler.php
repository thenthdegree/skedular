<?php
	//user wants ID to be remembered
	if(isset($_POST['studentid'])){
	    $rid =(string)$_POST['studentid'];
	    $timeset = 2147483647 ;
	    setcookie("remember_id", $rid, $timeset);
	} else {
	    die("<h1><center>Please do not access this page directly.");
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Skedular | NSBHS Timetable</title>

    <!-- Styling -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/sticky-footer.css" rel="stylesheet">
    <link href="css/table.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
<?php

$id = $_POST['studentid'];
$day = $_POST['day'];

if($day > 0 && $day <13) {
	$arrayofdays = array(1 => 'Monday A', 2 => 'Tuesday A', 3=> 'Wednesday A', 4=> 'Thursday A', 5 => 'Friday A', 6 => 'Monday B', 7 => 'Tuesday B', 8 => 'Wednesday B', 9 => 'Thursday B', 10 => 'Friday B', 11 => '', 12 => '');
	$dstr = $arrayofdays[$day];
} else {
	die('Invalid.');
}

if ($day == "11"){
	date_default_timezone_set('Australia/Sydney');
	if (date('W')%2==1){
		//week A
		$dstr = date('l'); $dstr = $dstr." A";
		$day = date('N');
		if ($day == 6 || $day == 7) {
			$dstr = "Monday B";
			$day = 6;	
		}
	} else {
		//week b
		$dstr = date('l'); $dstr = $dstr." B";
		$day = date('N');
		$day = $day+5;
		if ($day == 11 || $day == 12) {
			$dstr = "Monday A";
			$day = 1;	
		}
	}
};

if ($day == "12"){
	date_default_timezone_set('Australia/Sydney');
	if (date('W')%2==1){
		//week A
		$dstr = date('l', strtotime("+1 day")); $dstr = $dstr." A";
		$day = date('N');
		$day = $day+1;
		if ($day > 5) {
			$dstr = "Monday B";
			$day = 6;	
		}
	} else {
		//week B
		$dstr = date('l', strtotime("+1 day")); $dstr = $dstr." B";
		$day = date('N');
		$day = $day+6;
		if ($day > 10) {
			$dstr = "Monday A";
			$day = 1;	
		}
	}
};

//cache
date_default_timezone_set('Australia/Sydney');
$pragmapath = "cache/".$id."-".$day.".html";
$pragmatime = 60 * 60 * 24 * 5; //5 days
if (file_exists($pragmapath) && (time() - $pragmatime < filemtime($pragmapath))) {
	echo "<!-- nth° | Cached ".date('jS F Y H:i', filemtime($pragmapath))." -->";
	include($pragmapath);
	goto cached;
}

/* || CHECKING IF THE API IS UP IS NOT WORKING (ironically.)
$apilink = "http://api.skedular.co/?id=".$id."&day=".$day;

if(!is_readable($apilink)) { //checks for connection errors
	die("<center><h1>Could not connect to Skedular API.</h1><h2>Please contact an administrator.</h2>");
} else {
	$api = json_decode(file_get_contents("http://api.skedular.co/?id=$id&day=$day"));	
}
*/

$api = json_decode(file_get_contents("http://api.skedular.co/?id=$id&day=$day"));	

if(isset($api->error)) { //checks for errors thrown by the api
	die("<center><h1>An error occured with the request.</h1><br>Error Code: <b>". $api->error ."</b><br>Error Description: <b>". $api->desc ."</b>");
}

if ($day != 3 && $day != 8) {
	for ($i = 0; $i <= 6; $i++) {
		${"p".$i."c"} = $api[$i]->readableclass; ${"p".$i."t"} = $api[$i]->teacherid; ${"p".$i."r"} = $api[$i]->roomcode; ${"p".$i."s"} = $api[$i+7]->starttime; ${"p".$i."e"} = $api[$i+7]->endtime;
	}
} else {
	for ($i = 0; $i <= 3; $i++) {
		${"p".$i."c"} = $api[$i]->readableclass; ${"p".$i."t"} = $api[$i]->teacherid; ${"p".$i."r"} = $api[$i]->roomcode; ${"p".$i."s"} = $api[$i+4]->starttime; ${"p".$i."e"} = $api[$i+4]->endtime;
	}
}

//render table here
ob_start();
echo "

<div class=\"container\">
<div class=\"page-header\">
<center><h4>Timetable for $dstr</h4></center>
</div><center><table>";

if($p0c !== 'null') {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 0</span>
	<br>
	<span class=\"timefont\">$p0s - $p0e</span>
	</td>
	<td class=\"$p0c\">
	<span class=\"classbold\">$p0c</span>
	<br>
	with $p0t in $p0r
	</td>
	</tr> "; 
} else {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 0</span>
	<br>
	<span class=\"timefont\">$p0s - $p0e</span>
	</td>
	<td class=\"noclass\">
	<span class=\"classbold\">No Class</span>
	</td>
	</tr> ";
}

echo "
<tr>
<td class=\"filler\" colspan=\"2\">Roll Call</td>
</tr> ";

if($p1c !== 'Free') {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 1</span>
	<br>
	<span class=\"timefont\">$p1s - $p1e</span>
	</td>
	<td class=\"$p1c\">
	<span class=\"classbold\">$p1c</span>
	<br>
	with $p1t in $p1r
	</td>
	</tr>  "; 
} else {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 1</span>
	<br>
	<span class=\"timefont\">$p1s - $p1e</span>
	</td>
	<td class=\"noclass\">
	<span class=\"classbold\">Free Period</span>
	</td>
	</tr>  ";
}

if ($p2c !== 'Free') {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 2</span>
	<br>
	<span class=\"timefont\">$p2s - $p2e</span>
	</td>
	<td class=\"$p2c\">
	<span class=\"classbold\">$p2c</span>
	<br>
	with $p2t in $p2r
	</td>
	</tr>  ";
} else {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 2</span>
	<br>
	<span class=\"timefont\">$p2s - $p2e</span>
	</td>
	<td class=\"noclass\">
	<span class=\"classbold\">Free Period</span>
	</td>
	</tr>  ";
	
}

echo "
<tr>
<td class=\"filler\" colspan=\"2\">Recess</td>
</tr> ";
    
if ($p3c !== 'Free') {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 3</span>
	<br>
	<span class=\"timefont\">$p3s - $p3e</span>
	</td>
	<td class=\"$p3c\">
	<span class=\"classbold\">$p3c</span>
	<br>
	with $p3t in $p3r
	</td>
	</tr>  ";
} else {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 3</span>
	<br>
	<span class=\"timefont\">$p3s - $p3e</span>
	</td>
	<td class=\"noclass\">
	<span class=\"classbold\">Free Period</span>
	</td>
	</tr>  ";
	
}

if ($day != 3 && $day != 8) {
	if ($p4c !== 'Free') {
		echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 4</span>
	<br>
	<span class=\"timefont\">$p4s - $p4e</span>
	</td>
	<td class=\"$p4c\">
	<span class=\"classbold\">$p4c</span>
	<br>
	with $p4t in $p4r
	</td>
	</tr>  ";
	} else {
		echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 4</span>
	<br>
	<span class=\"timefont\">$p4s - $p4e</span>
	</td>
	<td class=\"noclass\">
	<span class=\"classbold\">Free Period</span>
	</td>
	</tr>  ";
		
	}
	
echo "
<tr>
<td class=\"filler\" colspan=\"2\">Lunch</td>
</tr>";
	
	if ($p5c !== 'Free') {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 5</span>
	<br>
	<span class=\"timefont\">$p5s - $p5e</span>
	</td>
	<td class=\"$p5c\">
	<span class=\"classbold\">$p5c</span>
	<br>
	with $p5t in $p5r
	</td>
	</tr>  ";
	} else {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 5</span>
	<br>
	<span class=\"timefont\">$p5s - $p5e</span>
	</td>
	<td class=\"noclass\">
	<span class=\"classbold\">Free Period</span>
	</td>
	</tr>  ";
		
	}
	
	if ($p6c !== 'Free') {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 6</span>
	<br>
	<span class=\"timefont\">$p6s - $p6e</span>
	</td>
	<td class=\"$p6c\">
	<span class=\"classbold\">$p6c</span>
	<br>
	with $p6t in $p6r
	</td>
	</tr>  ";
	} else {
	echo "
	<tr>
	<td class=\"periodstyle\">
	<span class=\"periodbold\">Period 6</span>
	<br>
	<span class=\"timefont\">$p6s - $p6e</span>
	</td>
	<td class=\"noclass\">
	<span class=\"classbold\">Free Period</span>
	</td>
	</tr>  ";		
	}
} else {
echo "
<tr>
<td class=\"filler\" colspan=\"2\">Lunch + Sport</td>
</tr>";	
}

$fp = fopen($pragmapath, 'w');
fwrite($fp, ob_get_contents());
fclose($fp);
ob_end_flush();

cached:
?>
	</table></center><br>
    <a href="/"><center><button class="btn btn-lg btn-primary btn">Select another day</button></a>
    </div><br>

    <div id="footer">
     <div class="container">
        <center><p class="text-muted">nth° &copy; 2014</p>
      </center></div>
    </div>
  </body>
</html>