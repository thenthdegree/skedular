<?php

class main {
		
		//variables
		public $id;
		public $day;
		
		//functions
		public function __construct() {
			if(isset($_GET['id']) && isset($_GET['day'])) {
				$this->verify();
			} else {
				$ehandler = new ehandler();
				$ehandler->error('001');
			}
		}
		
		private function verify() {
			if(strlen($_GET['id']) == 9 && filter_var($_GET['id'], FILTER_VALIDATE_INT) && $_GET['day'] <= 10 && $_GET['day'] >= 1 && filter_var($_GET['day'], FILTER_VALIDATE_INT)){
				$this->id = $_GET['id']; $this->day = $_GET['day'];
			} else {
				$ehandler = new ehandler();
				$ehandler->error('002');
			}
		}
		
}

class retrieve {
	
		//variables
		public $classes = array();
		public $times = array();
		private $query;
		private $main;
		public $free;
		
		//functions
		public function __construct(main $main) {
			$this->main = $main;
		    $this->query =  "SELECT DISTINCT readableclass,teacherid,roomcode,period
							 FROM ". dbconfig::get('apptable') ."
							 WHERE classcode IN (
							 SELECT classcode FROM ". dbconfig::get('membertable') . " WHERE studentid LIKE :id)
							 AND daynumber LIKE :day AND period NOT LIKE 'RC' ORDER BY period; ";
			$this->query(); $this->times();
		}
		
		private function query(){
			$dbq = dbquery::instantiate(); 
			try {
				$query = $dbq->handle->prepare($this->query);
				$query->bindParam(':id', $this->main->id, PDO::PARAM_INT); $query->bindParam(':day', $this->main->day, PDO::PARAM_INT);
				$query->execute(); $this->classes = $query->fetchall(PDO::FETCH_ASSOC); 
				$this->display();
			} catch (PDOException $e) {
				$ehandler = new ehandler();
				$ehandler->error('005');	
			}
		}

        private function display(){
            $this->process(); $this->free();
            if($this->main->day == "3" || $this->main->day == "8") { $this->shift(); }
			$this->times();
            $json = json_encode($this->classes, JSON_PRETTY_PRINT);
            print_r($json);
        }

		
		private function process() {
			if(sizeof($this->classes) == 0) {
				$ehandler = new ehandler();
				$ehandler->error('003');
			}
			if($this->classes[0]['period'] == 1 || $this->classes[0]['period'] == 2) {
				$array = array('readableclass' => 'null', 'teacherid' => 'null', 'roomcode' => 'null', 'period' => 0);
				array_unshift($this->classes, $array);
			}
		}
		
		private function free() { //this function is messy. calculates if student has a free period.
			$i = 0; $y = array(); if($this->main->day == "3" || $this->main->day == "8" ) { $p = 3; } else { $p = 6; };
			foreach($this->classes as &$x) {$y[] = $this->classes[$i]['period']; $i++;} $z = range(0, "$p");
			$this->free = array_diff($z, $y); $this->free = array_values($this->free); $size = sizeof($this->free); 
			if($size > 0) {
				$i = 0;
				foreach($this->free as &$x) {
					$index = $this->free[$i];
					$array = array('readableclass' => 'Free', 'teacherid' => 'null', 'roomcode' => 'null', 'period' => "$index");
					array_push($this->classes, $array);
					$i++;
				}
			usort($this->classes, function($a, $b) {return $a['period'] - $b['period'];});
			}
		}
		
		private function shift() {
			$magnitude = dbconfig::get('mag');
			
			switch($magnitude) {
				case 0:
				//no action needed
				break;	
				
				case 1:
				$this->classes[1]['period'] = 2; $this->classes[2]['period'] = 3; $this->classes[3]['period'] = 1;
				usort($this->classes, function($a, $b) {return $a['period'] - $b['period'];});
				break;
				
				case 2:
				$this->classes[1]['period'] = 3; $this->classes[2]['period'] = 1; $this->classes[3]['period'] = 2;
				usort($this->classes, function($a, $b) {return $a['period'] - $b['period'];});
				break;
			}
		}
	
		private function times() {
			$dbq = dbquery::instantiate();
				try {
					$query = $dbq->handle->prepare("select starttime, endtime from liss_bell_times where daynumber like :day and period not like 'rc' and period not like 'r' and period not like 'l1' and period not like 'l2' and period not like 'as'");
					$query->bindParam(':day', $this->main->day, PDO::PARAM_INT); $query->execute();
					$this->times = $query->fetchall(PDO::FETCH_ASSOC); 
				} catch (PDOException $e) {
					$ehandler = new ehandler();
					$ehandler->error('005');	
				}
			foreach($this->times as &$x) {
				$x['starttime'] = substr($x['starttime'], 0, -3);
				$x['endtime'] = substr($x['endtime'], 0, -3);
			}
			$this->classes = array_merge($this->classes, $this->times);
		}	
}

class ehandler {
	
	public $error = array('error' => '000', 'errort' => 'null', 'desc' => 'null');
	
	public function error($var) {
		switch ($var) {
			case 001:
			$desc = "One or more inputs missing.";
			$type = "Warning";
			break;	
			case 002:
			$desc = "Malformed inputs.";
			$type = "Warning";
			break;
			case 003:
			$desc = "ID not found.";
			$type = "Exception";
			break;
			case 004:
			$desc = "Could not establish a connection to the database. Please contact an Administrator.";
			$type = "Exception";
			break;
			case 005:
			$desc = "Could not query database with provided details.";
			$type = "Exception";
			break;
		}
		$error['error'] = "$var"; $error['errort'] = "$type"; $error['desc'] = "$desc";
		$error = json_encode($error, JSON_PRETTY_PRINT);
		die($error);
	}
}

class dbconfig {
		
		//variables
		static $store;
		
		//functions
		public static function get($var) {
			return self::$store[$var];	
		}
		
		public static function write($var, $write) {
			self::$store[$var] = $write;	
		}
	
}

class dbquery {
	
		//variables
		public $handle;
		private static $instance;
		
		//functions
		private function __construct() {
			$base = dbconfig::get('type') .":host=". dbconfig::get('host') .";dbname=". dbconfig::get('db').";port=". dbconfig::get('port') .";charset=". dbconfig::get('charset') ."";
			$user = dbconfig::get('user');
			$pass = dbconfig::get('pass');
			try {
				$this->handle = new PDO($base, $user, $pass);
				$this->handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $e) {
				$ehandler = new ehandler();
				$ehandler->error('004');
			}
			
		}
		
		public static function instantiate(){
				if (!isset(self::$instance))
				{
					$classname = __CLASS__;
					self::$instance = new $classname;
				}
				return self::$instance;
		}

}

?>