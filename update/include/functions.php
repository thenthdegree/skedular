<?php

class dbconfig {
		
		//variables
		static $store;
		
		//functions
		public static function get($var) {
			return self::$store[$var];	
		}
		
		public static function write($var, $write) {
			self::$store[$var] = $write;	
		}
	
}

//do not try to construct this class outside, will not work.
class dbquery {
	
		//variables
		public $handle;
		private static $instance;
		
		//functions
		private function __construct() {
			$base = dbconfig::get('type') .":host=". dbconfig::get('host') .";dbname=". dbconfig::get('db').";port=". dbconfig::get('port') .";charset=". dbconfig::get('charset') ."";
			$user = dbconfig::get('user');
			$pass = dbconfig::get('pass');
			$this->handle = new PDO($base, $user, $pass);
		}
		
		public static function instantiate(){
				if (!isset(self::$instance))
				{
					$classname = __CLASS__;
					self::$instance = new $classname;
				}
				return self::$instance;
		}


}

class update {
	
	private $pregarray = array(                         
		 'RC' => 'RollCall',
		 'IPT' => 'IPT',
		 'ENG' => 'English',
		 'EN\d' => 'English',
		 'ENX' => 'EnglishX',
		 'MAT' => 'Mathematics',
		 'MM\d' => 'Mathematics',
		 'SCI' => 'Science',
		 'GEO' => 'Geography',
		 'CAR' => 'Careers',
		 'VAR' => 'VisualArts',
		 'MU\S' => 'Music',
		 'DRA' => 'Drama',
		 'LIB' => 'Library',
		 'HIS' => 'History',
		 'HEX' => 'HistoryX',
		 'BST' => 'BusinessStudies',
		 'COM' => 'Commerce',
		 'AHI' => 'AncientH',
		 'ECO' => 'Economics',
		 'LST' => 'LegalStudies',
		 'MHI' => 'ModernHistory',
		 'HIX' => 'HistoryX',
		 'FRE' => 'French',
		 'GER' => 'German',
		 'IND' => 'Indonesian',
		 'JAP' => 'Japanese',
		 'LAT' => 'Latin',
		 'GEX' => 'GermanX',
		 'JAX' => 'JapaneseX',
		 'LAX' => 'LatinX',
		 'PDHPE' => 'PDHPE',
		 'PASS' => 'PASS',
		 'BIO' => 'Biology',
		 'CHE' => 'Chemistry',
		 'PHY' => 'Physics',
		 'DAT' => 'DesignTech',
		 'EST' => 'Engineering',
		 'FTE' => 'FoodTech',
		 'PSY' => 'PowerSystems',
		 'TDR' => 'TechDrawing',
		 'SDD' => 'SDD',
		 'GTE' => 'WoodTech');
	private $classes;
	
	static function execute($sql, $arg, $a = null) { //arg determines if the statement is used as a checking function or as a proper function
		$dbh = dbquery::instantiate();
		$query = $dbh->handle->prepare($sql);
		if(isset($a)){
				$query->bindParam(":id", $a['id'], PDO::PARAM_STR);
				$query->bindParam(":class", $a['classcode'], PDO::PARAM_STR);
		}
		$query->execute(); $error = $query->errorinfo();
		if($error[0] === '00000') {
			if($arg == true) { 
				return $query->fetchall(PDO::FETCH_ASSOC); //we want results from the command
			} else {
				return true; //command completed successfully but we dont want results
			}
		} else {
			if($arg == true) { 
				die(print_r($error)); //command needs to complete successfully else death
			} else {
				return false; //command unsuccessfully as a check measure
			}
		}
	}
	
	public function __construct() { //we want to make sure that we have the tables we need for the script
		if(self::execute("SELECT * FROM ". dbconfig::get('apptable') ."", false)) {
			self::execute("DROP TABLE ". dbconfig::get('apptable') ."", false);
		}
			self::execute("CREATE TABLE ". dbconfig::get('apptable') ." LIKE ". dbconfig::get('lisstable') ."", true);
			self::execute("INSERT INTO ". dbconfig::get('apptable') ." (SELECT * FROM ". dbconfig::get('lisstable') .")", true);
			self::execute("ALTER TABLE ". dbconfig::get('apptable') ."
						   ADD COLUMN ReadableClass tinytext,
						   DROP COLUMN timetable_id,
						   DROP COLUMN ttstructure,
						   DROP COLUMN created_at,
						   DROP COLUMN updated_at;", true);
		echo "<center><h1>Table Prepped."; $this->main();
	}
	
	private function main() {
		$this->classes = self::execute("SELECT classcode,id FROM ". dbconfig::get('lisstable') ." ORDER BY ID", true);
		foreach($this->pregarray as $key => $value) {
			foreach($this->classes as &$a) {
				$a = preg_replace("/\S?\S?$key\s\S?\S?\S?/", "$value", $a);
			}
		} 
		foreach($this->classes as $a) {
			self::execute("UPDATE ". dbconfig::get('apptable') ." SET ReadableClass=:class WHERE id=:id", true, $a);
		}
		echo "<br>Records inserted successfully.";
	}
}
