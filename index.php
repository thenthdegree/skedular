<?php  
if(isset( $_COOKIE['remember_id'])) 
{ 
	$id =(string)$_COOKIE['remember_id']; 
	$autofocust ="";
	$autofocuss ="";
} else {
	$id = "";
	$autofocust = "";
	$autofocuss ="autofocus";
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Skedular | NSBHS Timetable</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
    <link href="css/sticky-footer.css" rel="stylesheet">
    <link href="css/bootstrap-select.min.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body>

    <div class="container">

      <form class="form-signin" role="form" method="POST" action="handler.php">
        <center><h2 class="form-signin-heading">Skedular</h2>
        		<h5 style="color:#999999;">Release Candidate 2</h5></center>
        <center><input type="text" name="studentid" id="textfield" placeholder="Student ID" class="form-control" pattern=".{9,9}" required title="Student ID is made of 9 characters" value="<?php echo $id; echo '"'; echo " $autofocust"; ?> required></center>
       		<select name="day" class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="9" <?php echo $autofocuss; ?>>
            	<optgroup label="Relative">
                <option value="11" title="Today">Today</option>
                <option value="12" title="Tomorrow">Tomorrow</option>
            	<optgroup label="Week A">
                <option value="1" title="Monday A">Monday</option>
                <option value="2" title="Tuesday A">Tuesday</option>
                <option value="3" title="Wednesday A">Wednesday</option>
                <option value="4" title="Thursday A">Thursday</option>
                <option value="5" title="Friday A">Friday</option>
                <optgroup label="Week B">
                <option value="6" title="Monday B">Monday</option>
                <option value="7" title="Tuesday B">Tuesday</option>
                <option value="8" title="Wednesday B">Wednesday</option>
                <option value="9" title="Thursday B">Thursday</option>
                <option value="10" title="Friday B">Friday</option>
        		</select>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
      </form>

    </div> <!-- /container -->
    
    <div id="footer">
      <div class="container">
        <center><p class="text-muted">nth° © 2014</p>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
	<script type="text/javascript">
	window.onload=function(){
	    $('select').selectpicker();
	};
	</script>
  </body>
</html>
